var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

var config = {
    entry: {
        app: './dev/func.ts'
    },

    resolve: {
        extensions: ['', '.ts']
    },

    module: {
        loaders: [
            {
                test: /\.ts$/,
                loader: 'ts'
            }
        ]
    },

    plugins: [
        // new webpack.optimize.CommonsChunkPlugin({
        //     name: ['app']
        // }),

        // new HtmlWebpackPlugin({
        //     template: 'dev/index.html'
        // })
    ]
};

module.exports = webpackMerge(config, {
    output: {
        path: './output',
        publicPath: './',
        filename: '[name].js',
    },

    htmlLoader: {
        minimize: true
    },

    plugins: [
        new webpack.NoErrorsPlugin(),
        new webpack.optimize.DedupePlugin(),
        // new webpack.optimize.UglifyJsPlugin({
        //     compress: {
        //         warnings: false
        //     },
        //     mangle: {
        //         keep_fnames: true
        //     }
        // })
    ]
});
