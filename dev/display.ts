import {tia} from './tia';
import {cpu} from './cpu';
import {pia} from './pia';

import {Common} from './common';
import {rom} from './rom';
import {register} from './registers';

class Display {

	// Private

	// canvas.width = 160;

	// canvas.height = 192;

	ctx: any;

	w: number;

	h: number;

	data: any;

	scanline: number = 0;

	clock: number = 0;

	init(canvas: any) {

		this.ctx = canvas.getContext('2d');

		this.w = canvas.width;

		this.h = canvas.height;

		this.data = this.ctx.getImageData(0, 0, this.w, this.h);

		this.ctx.fillStyle = 'black';

		this.ctx.fillRect(0, 0, this.w, this.h);

	};

	public nextFrame() {


	var promise = new Promise((resolve: any, reject: any) => {

	try {
		var cpuCounter = 0;
		for(this.scanline = 0; this.scanline < 70; this.scanline++) { // 40 // 4
			for(this.clock = 0; this.clock < 228; this.clock += 3) {
				cpu.pulse();
				pia.clockPassed(3);
			}
			cpu.unlock();
		}
	
		for(this.scanline = 0; this.scanline < 192; this.scanline++) { // 162
				for(this.clock = 0; this.clock < 68; this.clock += 3) {
					cpu.pulse();
					pia.clockPassed(3);
				}
				
				for(this.clock = 0; this.clock < 160; this.clock++) {

					var pf0 = Common.toBinaryReverseArray(tia.playfield.pf0);
					var pf1 = Common.toBinaryReverseArray(tia.playfield.pf1);
					var pf2 = Common.toBinaryReverseArray(tia.playfield.pf2);

					// if(tia.vsync == false) {
					// 	console.log('falsed');
					// 	continue;
					// }

					//console.log(tia.playfield.color);

					if(this.clock <= 16) {
						if(this.clock < 4) {
							if(pf0[4] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock <= 8) {
							if(pf0[5] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock <= 12) {
							if(pf0[6] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock <= 16) {
							if(pf0[7] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						}
					} else if(this.clock > 16 && this.clock <= 48) {
						if(this.clock > 16 && this.clock < 20) {
							if(pf1[7] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock >= 20 && this.clock < 24) {
							if(pf1[6] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock >= 24 && this.clock < 28) {
							if(pf1[5] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock >= 28 && this.clock < 32) {
							if(pf1[4] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock >= 32 && this.clock < 36) {
							if(pf1[3] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock >= 36 && this.clock < 40) {
							if(pf1[2] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock >= 40 && this.clock < 44) {
							if(pf1[1] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock >= 44 && this.clock < 48) {
							if(pf1[0] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						}
					} else if(this.clock > 48 && this.clock < 80) {
						if(this.clock > 48 && this.clock < 52) {
							if(pf2[0] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 52 && this.clock < 56) {
							if(pf2[1] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 56 && this.clock < 60) {
							if(pf2[2] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 60 && this.clock < 64) {
							if(pf2[3] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 64 && this.clock < 68) {
							if(pf2[4] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 68 && this.clock < 72) {
							if(pf2[5] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 72 && this.clock < 76) {
							if(pf2[6] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 76 && this.clock < 80) {
							if(pf2[7] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						}
					} else if(this.clock > 80 && this.clock < 96 && tia.playfield.reflect == 0) { // REF = 0
						if(this.clock > 80 && this.clock < 84) {
							if(pf0[4] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 84 && this.clock < 88) {
							if(pf0[5] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 88 && this.clock < 92) {
							if(pf0[6] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 92 && this.clock < 96) {
							if(pf0[7] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						}
					} else if(this.clock > 96 && this.clock < 128 && tia.playfield.reflect == 0) {
						if(this.clock > 96 && this.clock < 100) {
							if(pf1[7] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 100 && this.clock < 104) {
							if(pf1[6] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 104 && this.clock < 108) {
							if(pf1[5] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 108 && this.clock < 112) {
							if(pf1[4] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 112 && this.clock < 116) {
							if(pf1[3] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 116 && this.clock < 120) {
							if(pf1[2] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 120 && this.clock < 124) {
							if(pf1[1] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 124 && this.clock < 128) {
							if(pf1[0] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						}
					} else if(this.clock > 128 && this.clock < 160 && tia.playfield.reflect == 0) {
						if(this.clock > 128 && this.clock < 132) {
							if(pf2[0] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 132 && this.clock < 136) {
							if(pf2[1] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 136 && this.clock < 140) {
							if(pf2[2] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 140 && this.clock < 144) {
							if(pf2[3] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 144 && this.clock < 148) {
							if(pf2[4] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 148 && this.clock < 152) {
							if(pf2[5] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 152 && this.clock < 156) {
							if(pf2[6] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 156 && this.clock < 160) {
							if(pf2[7] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						}
					} else if(this.clock > 80 && this.clock < 112 && tia.playfield.reflect == 1) { // REF = 1
						if(this.clock > 80 && this.clock < 84) {
							if(pf2[7] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 84 && this.clock < 88) {
							if(pf2[6] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 88 && this.clock < 92) {
							if(pf2[5] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 92 && this.clock < 96) {
							if(pf2[4] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 96 && this.clock < 100) {
							if(pf2[3] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 100 && this.clock < 104) {
							if(pf2[2] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 104 && this.clock < 108) {
							if(pf2[1] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 108 && this.clock < 112) {
							if(pf2[0] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						}
					} else if(this.clock > 112 && this.clock < 144 && tia.playfield.reflect == 1) {
						if(this.clock > 112 && this.clock < 116) {
							if(pf1[0] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 116 && this.clock < 120) {
							if(pf1[1] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 120 && this.clock < 124) {
							if(pf1[2] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 124 && this.clock < 128) {
							if(pf1[3] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 128 && this.clock < 132) {
							if(pf1[4] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 132 && this.clock < 136) {
							if(pf1[5] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 136 && this.clock < 140) {
							if(pf1[6] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 140 && this.clock < 144) {
							if(pf1[7] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						}
					} else if(this.clock > 144 && this.clock < 160 && tia.playfield.reflect == 1) {
						if(this.clock > 144 && this.clock < 148) {
							if(pf0[7] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 148 && this.clock < 152) {
							if(pf0[6] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 152 && this.clock < 156) {
							if(pf0[5] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						} else if(this.clock > 156 && this.clock < 160) {
							if(pf0[4] == '1') {
								var c = Common.convertHex(tia.playfield.color);
							} else {
								var c = Common.convertHex(tia.background.color);
							}
						}
					}

					var pixelindex = Math.floor((this.scanline * this.w + ( 2 * this.clock)) * 4);
					this.data.data[pixelindex] = c[0];
					this.data.data[pixelindex + 1] = c[1];
					this.data.data[pixelindex + 2] = c[2];
					
					var pixelindex = Math.floor((this.scanline * this.w + ( 2 * this.clock - 1)) * 4);
					this.data.data[pixelindex] = c[0];
					this.data.data[pixelindex + 1] = c[1];
					this.data.data[pixelindex + 2] = c[2];

					
					cpuCounter++;
					if(cpuCounter > 2) { //3
						cpu.pulse();
						cpuCounter = 0;
					}
					pia.clockPassed();
				}

			cpu.unlock();
		}

		this.ctx.putImageData(this.data, 0, 0);
		console.log(this.data, this.ctx);
		// for(this.scanline = 0; this.scanline < 30; this.scanline++) {
		// 	for(this.clock = 0; this.clock < 228; this.clock += 3) {
		// 		cpu.pulse();
		// 		pia.clockPassed(3);
		// 	}
		// 	cpu.unlock();
		// }

		//console.log(1);
		resolve(true)

		} catch(e) {
			reject({
				pc: register.PC,
				opcode: rom.file[register.PC].toString(16).toUpperCase(),
				error: e
			});
		}		
		});
		return promise;
	};
};

export var display: Display = new Display();