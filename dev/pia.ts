import {memory} from './memory';

class PIA {

	counter: number = 0;

	public setTimer(val: number) {
		memory[0x0284].value = val;
	};

	public clockPassed(val?: number) {
	
		if(val) {
			memory[0x0284].value -= val;
		} else {
			memory[0x0284].value--;
		}
	};

}

export var pia: PIA = new PIA();