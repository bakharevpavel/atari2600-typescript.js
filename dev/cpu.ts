import {opcode} from './opcodes';
import {rom} from './rom';
import {register} from './registers';

class CPU {

	// Private

	locked: boolean = false;

	cycle: number = 0;

	public lock() {
		this.locked = true;
	};

	public unlock() {
		this.locked = false;
		this.cycle = 0; // ????
	};

	public pulse() {

		if(this.locked == false && this.cycle <= 0) {
			console.log(rom.file[register.PC].toString(16).toUpperCase());
			opcode[rom.file[register.PC].toString(16).toUpperCase()](this);
			register.PC++;
		}
		this.cycle--;
	};

	public setCycle(val: number) {
		this.cycle = val;
	};
};

export var cpu: CPU = new CPU();