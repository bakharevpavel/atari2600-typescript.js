export class Common {
	public static swap16(val: number): number {
		return ((val & 0xFF) << 8) | ((val >> 8) & 0xFF);
	};

	public static toBinary(val: any) {
		val = val.toString(2).replace('-', ''); // Borked
		if(val.length < 8) {
			for(var i = 0, length = val.length; i < 8 - length; i++) {
				val = '0' + val;
			}
		}
		return val;
	};

	public static toBinaryArray(val: any) {
		return this.toBinary(val).split('');
	}

	public static toBinaryReverse(val: any) {
		val = val.toString(2).replace('-', ''); // Borked
		if(val.length < 8) {
			for(var i = 0, length = val.length; i < 8 - length; i++) {
				val = '0' + val;
			}
		}
		return val.split('').reverse().join('');
	};

	public static toBinaryReverseArray(val: any) {
		return this.toBinaryReverse(val).split('');
	};

	public static convertHex(hex: any){
		var c = [];
    	hex = hex.replace('#','');
    	c[0] = parseInt(hex.substring(0,2), 16);
    	c[1] = parseInt(hex.substring(2,4), 16);
   		c[2] = parseInt(hex.substring(4,6), 16);
    	return c;
	};

};