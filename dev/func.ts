// import './flags';
import {cpu} from './cpu';
import {tia} from './tia';
import {pia} from './pia';
import {display} from './display';
import {Common} from './common';
// import './memory';
// import './registers';
// import './opcodes';

import {rom} from './rom';

var scanlineCounter = 0;

// var cpu = new CPU();
// var tia = new TIA(document.querySelector('canvas'));
// var pia = new PIA();
// var display = new Display(document.querySelector('canvas'), tia);


(<HTMLInputElement>document.querySelector('#file')).onchange = processFile;

display.init(document.querySelector('canvas'));
tia.init(document.querySelector('canvas'));

var requestAnimationFrame = window.requestAnimationFrame;
window.requestAnimationFrame = requestAnimationFrame;


function processFile()  {
	console.log('Reading process started!');

	var file = (<HTMLInputElement>document.getElementById('file')).files[0];

	var reader = new FileReader();
	reader.readAsArrayBuffer(file);

	reader.onloadend = function(evt) {
		var data = this.result;
	
		// Lets mirror our data to match 64K rom

		rom.file = new Uint8Array(65536);
		rom.romSize = data.byteLength
		for(var i = 0; i < 65536; i += rom.romSize) {
			rom.file.set((new Uint8Array(data)), i);
			console.log(123);
		}
		read();
	}
}

function read() {
	var clock = 0;
	//requestAnimationFrame(display.nextFrame);

	// function nextFrame() {
	// 	setTimeout(function() {
	// 		requestAnimationFrame(display.nextFrame);
	// 	}, 1000 / 60);
	// 	nextFrame();
	// }
	

	function nextFrame() {
		//setTimeout(function() {
			//requestAnimationFrame(display.nextFrame);
			display.nextFrame().then(function() {
				//console.log(2);
				setTimeout(function() {
					nextFrame();
				}, 1000);
				
			}).catch(function(data: any) {
				console.log(data.pc);
				console.log(data.opcode);
				throw data.error;
			});
			
		//}, 1000 / 60 /* / 60 */);
		
	}

	nextFrame();

};