// Check if conditional branches should reset their respective flags to the specific value
// Move flag check to separate function

import {Common} from './common';
import {memory} from './memory';
import {flags} from './flags';
import {rom} from './rom';
import {register} from './registers';

import {tia} from './tia';
import {cpu} from './cpu';
import {pia} from './pia';

function createMemoryIfNotExist(addr: any) {
	if(typeof memory[addr] == 'undefined') {
		memory[addr] = {};
		memory[addr].write = function(val: any) {
			this.value = val;
		};
		memory[addr].value = 0;
	}
}

function isNextPage(pc1: any, pc2: any) { // Borked?
	return ('000' + pc1.toString(16)).slice(-4).split('')[0] != ('000' + pc2.toString(16)).slice(-4).split('')[0];
}


export var opcode = {
	'01': function() {
		var addr = rom.file[++register.PC] + register.X;
		createMemoryIfNotExist(addr);
		register.A = register.A || memory[memory[addr]].value;
		cpu.setCycle(6);
	},
	'9': function() { // ORA #nn
		register.A = rom.file[++register.PC] || register.A;
		cpu.setCycle(2);
		if(register.A == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.A < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}		
	},
	'10': function() { // BPL nnn
		if(flags.N == 1) {
			register.PC++;
			cpu.setCycle(2);
		} else {
			var num = rom.file[++register.PC];
			if (num > 127) {
				num = num - 256;
			}
			cpu.setCycle(3 + (isNextPage(register.PC, register.PC += num)? 1 : 0));
		}
	},
	'4C': function() { // JMP nnnn
		var addr: any = Common.swap16(((rom.file[++register.PC] & 0xFF) << 8) | (rom.file[++register.PC] & 0xFF));
		console.log(addr);

		addr = addr.toString(16).split('');
		console.log(addr);
		//throw new Error();
		var num = parseInt(addr.join(''), 16) - 1;
		register.PC = num;
		cpu.setCycle(3);
	},
	'29': function() { // AND #nn
		register.A = rom.file[++register.PC] && register.A;
		cpu.setCycle(2);
		if(register.A == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.A < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'78': function() { // SEI
		flags.I = 1;
		cpu.setCycle(2);
	},
	// '81': function() { // STA (nn, X)
	// 	var addr = rom.file[++register.PC] + register.X;
	// 	createMemoryIfNotExist(addr);
	// 	memory[memory[addr]].write(register.A);
	// 	cpu.setCycle(6);
	// 	console.log('asdadsa')
	// },
	'84': function() { // STY
		var addr = rom.file[++register.PC];
		createMemoryIfNotExist(addr);
		memory[addr].write(register.Y);
		cpu.setCycle(3);
	},
	'85': function() { // STA nn
		var addr = rom.file[++register.PC];
		createMemoryIfNotExist(addr);
		memory[addr].write(register.A);
		cpu.setCycle(3);
	},
	'86': function() { // STX nn
		var addr = rom.file[++register.PC];
		createMemoryIfNotExist(addr);
		memory[addr].write(register.X);
		cpu.setCycle(3);
	},
	'88': function() { // DEY
		register.Y--;
		cpu.setCycle(2);
		if(register.Y == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.Y < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'8A': function() { // TXA
		register.A = register.X;
		cpu.setCycle(2);
		if(register.A == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.A < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'8D': function() { // STA nnnn
		var addr = Common.swap16((((rom.file[++register.PC] & 0xff) << 8) | (rom.file[++register.PC] & 0xff)));
		createMemoryIfNotExist(addr);
		memory[addr].write(register.A);
		cpu.setCycle(4);
	},
	'90': function() {
		if(flags.C == 1) {
			register.PC++;
			cpu.setCycle(2);
		} else {
			var num = rom.file[++register.PC];
			if (num > 127) {
				num = num - 256;
			}
			cpu.setCycle(3 + (isNextPage(register.PC, register.PC += num)? 1 : 0));
		}
	},
	'95': function() { // STA nn, X
		var addr = rom.file[++register.PC] + register.X;
		createMemoryIfNotExist(addr);
		memory[addr].write(register.A);
		cpu.setCycle(4);
	},
	'9A': function() { // TXS
		register.S = register.X;
		cpu.setCycle(2);
	},
	'A0': function() { // LDY
		register.Y = rom.file[++register.PC];
		cpu.setCycle(2);
		if(register.Y == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.Y < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'A2': function() { // LDX
		register.X = rom.file[++register.PC];
		cpu.setCycle(2);
		if(register.X == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.X < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'A5': function() { // LDA nn
		var addr = rom.file[++register.PC];
		createMemoryIfNotExist(addr); // Error?
		register.A = memory[addr].value;
		console.log('-----------');
		console.log(register.PC);
		cpu.setCycle(3);
		if(register.A == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.A < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'A6': function() { // LDX nn
		var addr = rom.file[++register.PC];
		createMemoryIfNotExist(addr);
		register.X = memory[addr].value;
		cpu.setCycle(3);
		// console.log(memory[addr].value);
		// console.log(memory[addr].value.toString(2));
		// clearInterval(213123);
		if(register.X == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.X < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'A9': function() { // LDA
		register.A = rom.file[++register.PC];
		cpu.setCycle(2);
		if(register.A == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.A < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'AA': function() { // TAX
		register.X = register.A;
		cpu.setCycle(2);
		if(register.X == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.X < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'AD': function() { // LDA nnnn
		var addr = Common.swap16((((rom.file[++register.PC] & 0xff) << 8) | (rom.file[++register.PC] & 0xff)));
		//createMemoryIfNotExist(addr);
		register.A = memory[addr].value;
		cpu.setCycle(4);
		if(register.A == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.A < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	// 'B1': function() { // LDA (nn),Y
	// 	var addr = memory[rom.file[++register.PC]] + register.Y;
	// 	createMemoryIfNotExist(addr); // Error?
	// 	register.A = memory[addr].value;
	// 	console.log('-----------');
	// 	console.log(register.PC);
	// 	cpu.setCycle(5 + (isNextPage(register.PC, register.PC += num)? 1 : 0));
	// 	if(register.A == 0) {
	// 		flags.Z = 1;
	// 	} else {
	// 		flags.Z = 0;
	// 	}
	// 	if(register.A < 0) {
	// 		flags.N = 1;
	// 	} else {
	// 		flags.N = 0;
	// 	}
	// },
	'BD': function() { // LDA nnnn, x // Possible playground fall error?
		var d = ((rom.file[++register.PC] & 0xff) << 8) | (rom.file[++register.PC] & 0xff);
		//console.log(d + register.X);
		//clearInterval(dasda);
		var addr = Common.swap16(d) + register.X;
		createMemoryIfNotExist(addr);
		register.A = rom.file[addr]; // rom.file[addr]
		console.log('ssssssssss' + register.A);
		cpu.setCycle(4); // Add cycle
		if(register.A == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.A < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}	
	},
	'C6': function() { // DEC nn
		var addr = rom.file[++register.PC];
		createMemoryIfNotExist(addr);
		memory[addr].value--;
		cpu.setCycle(5);
		if(memory[addr].value == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(memory[addr].value < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}	
	},
	'C9': function() {
		var comparable = rom.file[++register.PC];
		cpu.setCycle(2);
		if(register.A - comparable == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.A - comparable < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
		if(register.A - comparable >= 0) {
			flags.C = 1;
		} else {
			flags.C = 0;
		}
	},
	'CA': function() { // DEX
		register.X--;
		cpu.setCycle(2);
		if(register.X == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.X < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'D0': function() { // BNE
		if(flags.Z == 1) {
			register.PC++;
			cpu.setCycle(2); // Add cycle
		} else {
			var num = rom.file[++register.PC];
			if (num > 127) {
				num = num - 256;
			}
			cpu.setCycle(3 + (isNextPage(register.PC, register.PC += num)? 1 : 0));
		}
	},
	'D8': function() { // CLD
		flags.D8 = 0;
		cpu.setCycle(2);
	},
	'E6': function() { // INC nn
		var addr = rom.file[++register.PC];
		createMemoryIfNotExist(addr);
		memory[addr].value++;
		cpu.setCycle(5);
		if(memory[addr].value == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(memory[addr].value < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}	
	},
	'E8': function() { // INCX
		register.X++;
		cpu.setCycle(2);
		if(register.X == 0) {
			flags.Z = 1;
		} else {
			flags.Z = 0;
		}
		if(register.X < 0) {
			flags.N = 1;
		} else {
			flags.N = 0;
		}
	},
	'EA': function() { // NOP
		cpu.setCycle(2);
	},
	'F0': function() { // BEQ
		if(flags.Z == 0) {
			register.PC++;
			console.log('jump');
			cpu.setCycle(2); // Add cycle
		} else {
			var num = rom.file[++register.PC];
			if (num > 127) {
				num = num - 256;
			}
			console.log('jumped');
			cpu.setCycle(3 + (isNextPage(register.PC, register.PC += num)? 1 : 0));
		}		
	} 
};

// export var opcode = {
// 	'01': function() {
// 		var addr = rom.file[++register.PC] + register.X;
// 		createMemoryIfNotExist(addr);
// 		register.A = register.A || memory[memory[addr]].value;
// 		cpu.setCycle(6);
// 	},
// 	'9': function() { // ORA #nn
// 		register.A = rom.file[++register.PC] || register.A;
// 		cpu.setCycle(2);
// 		if(register.A == 0) {
// 			flags.Z = 1;
// 		} else {
// 			flags.Z = 0;
// 		}
// 		if(register.A < 0) {
// 			flags.N = 1;
// 		} else {
// 			flags.N = 0;
// 		}		
// 	},
// 	'A': function() { //Error
// 		var orig = register.A;
// 		register.A = register.A << 1;
// 		if(register.A == 0) {
// 			flags.Z = 1;
// 		} else {
// 			flags.Z = 0;
// 		}
// 		if(register.A < 0) {
// 			flags.N = 1;
// 		} else {
// 			flags.N = 0;
// 		}
// 		if(register.A > 0xFF && orig <= 0xFF) {
// 			flags.C = 1;
// 		} else {
// 			flags.C = 0;
// 		}
// 		cpu.setCycle(2);
// 	},
// 	'10': function() { // BPL nnn
// 		if(flags.N == 1) {
// 			register.PC++;
// 			cpu.setCycle(2);
// 		} else {
// 			var num = rom.file[++register.PC];
// 			if (num > 127) {
// 				num = num - 256;
// 			}
// 			cpu.setCycle(3 + (isNextPage(register.PC, register.PC += num)? 1 : 0));
// 		}
// 	},
// 	'4A': function() { //Error
// 		var orig = register.A;
// 		register.A = register.A >>> 1;
// 		if(register.A == 0) {
// 			flags.Z = 1;
// 		} else {
// 			flags.Z = 0;
// 		}
// 		if(register.A < 0) {
// 			flags.N = 1;
// 		} else {
// 			flags.N = 0;
// 		}
// 		if(register.A > 0xFF && orig <= 0xFF) {
// 			flags.C = 1;
// 		} else {
// 			flags.C = 0;
// 		}
// 		cpu.setCycle(2);
// 	},
// 	'4C': function() { // JMP nnnn
// 		var addr: any = Common.Common.swap16(((rom.file[++register.PC] & 0xFF) << 8) | (rom.file[++register.PC] & 0xFF));
// 		console.log(addr);

// 		addr = addr.toString(16).split('');
// 		console.log(addr);
// 		//throw new Error();
// 		var num = parseInt(addr.join(''), 16) - 1;
// 		register.PC = num;
// 		cpu.setCycle(3);
// 	},
// 	'29': function() { // AND #nn
// 		register.A = rom.file[++register.PC] && register.A;
// 		cpu.setCycle(2);
// 		if(register.A == 0) {
// 			flags.Z = 1;
// 		} else {
// 			flags.Z = 0;
// 		}
// 		if(register.A < 0) {
// 			flags.N = 1;
// 		} else {
// 			flags.N = 0;
// 		}
// 	},
// 	'78': function() { // SEI
// 		flags.I = 1;
// 		cpu.setCycle(2);
// 	},
// 	// '81': function() { // STA (nn, X)
// 	// 	var addr = rom.file[++register.PC] + register.X;
// 	// 	createMemoryIfNotExist(addr);
// 	// 	memory[memory[addr]].write(register.A);
// 	// 	cpu.setCycle(6);
// 	// 	console.log('asdadsa')
// 	// },
// 	'84': function() { // STY
// 		var addr = rom.file[++register.PC];
// 		createMemoryIfNotExist(addr);
// 		memory[addr].write(register.Y);
// 		cpu.setCycle(3);
// 	},
// 	'85': function() { // STA nn
// 		var addr = rom.file[++register.PC];
// 		createMemoryIfNotExist(addr);
// 		memory[addr].write(register.A);
// 		cpu.setCycle(3);
// 	},
// 	'86': function() { // STX nn
// 		var addr = rom.file[++register.PC];
// 		createMemoryIfNotExist(addr);
// 		memory[addr].write(register.X);
// 		cpu.setCycle(3);
// 	},
// 	'88': function() { // DEY
// 		register.Y--;
// 		cpu.setCycle(2);
// 		if(register.Y == 0) {
// 			flags.Z = 1;
// 		} else {
// 			flags.Z = 0;
// 		}
// 		if(register.Y < 0) {
// 			flags.N = 1;
// 		} else {
// 			flags.N = 0;
// 		}
// 	},
// 	'8A': function() { // TXA
// 		register.A = register.X;
// 		cpu.setCycle(2);
// 		if(register.A == 0) {
// 			flags.Z = 1;
// 		} else {
// 			flags.Z = 0;
// 		}
// 		if(register.A < 0) {
// 			flags.N = 1;
// 		} else {
// 			flags.N = 0;
// 		}
// 	},
// 	'8D': function() { // STA nnnn
// 		var addr = Common.Common.swap16((((rom.file[++register.PC] & 0xff) << 8) | (rom.file[++register.PC] & 0xff)));
// 		createMemoryIfNotExist(addr);
// 		memory[addr].write(register.A);
// 		cpu.setCycle(4);
// 	},
// 	'90': function() {
// 		if(flags.C == 1) {
// 			register.PC++;
// 			cpu.setCycle(2);
// 		} else {
// 			var num = rom.file[++register.PC];
// 			if (num > 127) {
// 				num = num - 256;
// 			}
// 			cpu.setCycle(3 + (isNextPage(register.PC, register.PC += num)? 1 : 0));
// 		}
// 	},
// 	'95': function() { // STA nn, X
// 		var addr = rom.file[++register.PC] + register.X;
// 		createMemoryIfNotExist(addr);
// 		memory[addr].write(register.A);
// 		cpu.setCycle(4);
// 	},
// 	'9A': function() { // TXS
// 		register.S = register.X;
// 		cpu.setCycle(2);
// 	},
// 	'A0': function() { // LDY
// 		register.Y = rom.file[++register.PC];
// 		cpu.setCycle(2);
// 		if(register.Y == 0) {
// 			flags.Z = 1;
// 		} else {
// 			flags.Z = 0;
// 		}
// 		if(register.Y < 0) {
// 			flags.N = 1;
// 		} else {
// 			flags.N = 0;
// 		}
// 	},
// 	'A2': function() { // LDX
// 		register.X = rom.file[++register.PC];
// 		cpu.setCycle(2);
// 		if(register.X == 0) {
// 			flags.Z = 1;
// 		} else {
// 			flags.Z = 0;
// 		}
// 		if(register.X < 0) {
// 			flags.N = 1;
// 		} else {
// 			flags.N = 0;
// 		}
// 	},
// 	'A5': function() { // LDA nn
// 		var addr = rom.file[++register.PC];
// 		//createMemoryIfNotExist(addr); // Error?
// 		register.A = memory[addr].value;
// 		console.log('-----------');
// 		console.log(register.PC);
// 		cpu.setCycle(3);
// 		if(register.A == 0) {
// 			flags.Z = 1;
// 		} else {
// 			flags.Z = 0;
// 		}
// 		if(register.A < 0) {
// 			flags.N = 1;
// 		} else {
// 			flags.N = 0;
// 		}
// 	},
// 	'A6': function() { // LDX nn
// 		var addr = rom.file[++register.PC];
// 		createMemoryIfNotExist(addr);
// 		register.X = memory[addr].value;
// 		cpu.setCycle(3);
// 		// console.log(memory[addr].value);
// 		// console.log(memory[addr].value.toString(2));
// 		// clearInterval(213123);
// 		if(register.X == 0) {
// 			flags.Z = 1;
// 		} else {
// 			flags.Z = 0;
// 		}
// 		if(register.X < 0) {
// 			flags.N = 1;
// 		} else {
// 			flags.N = 0;
// 		}
// 	},
// 	'A9': function() { // LDA
// 		register.A = rom.file[++register.PC];
// 		cpu.setCycle(2);
// 		if(register.A == 0) {
// 			flags.Z = 1;
// 		} else {
// 			flags.Z = 0;
// 		}
// 		if(register.A < 0) {
// 			flags.N = 1;
// 		} else {
// 			flags.N = 0;
// 		}
// 	},
// 	'AA': function() { // TAX
// 		register.X = register.A;
// 		cpu.setCycle(2);
// 		if(register.X == 0) {
// 			flags.Z = 1;
// 		} else {
// 			flags.Z = 0;
// 		}
// 		if(register.X < 0) {
// 			flags.N = 1;
// 		} else {
// 			flags.N = 0;
// 		}
// 	},
// 	'AD': function() { // LDA nnnn
// 		var addr = Common.Common.swap16((((rom.file[++register.PC] & 0xff) << 8) | (rom.file[++register.PC] & 0xff)));
// 		//createMemoryIfNotExist(addr);
// 		register.A = memory[addr].value;
// 		cpu.setCycle(4);
// 		if(register.A == 0) {
// 			flags.Z = 1;
// 		} else {
// 			flags.Z = 0;
// 		}
// 		if(register.A < 0) {
// 			flags.N = 1;
// 		} else {
// 			flags.N = 0;
// 		}
// 	},
// 	// 'B1': function() { // LDA (nn),Y
// 	// 	var addr = memory[rom.file[++register.PC]] + register.Y;
// 	// 	createMemoryIfNotExist(addr); // Error?
// 	// 	register.A = memory[addr].value;
// 	// 	console.log('-----------');
// 	// 	console.log(register.PC);
// 	// 	cpu.setCycle(5 + (isNextPage(register.PC, register.PC += num)? 1 : 0));
// 	// 	if(register.A == 0) {
// 	// 		flags.Z = 1;
// 	// 	} else {
// 	// 		flags.Z = 0;
// 	// 	}
// 	// 	if(register.A < 0) {
// 	// 		flags.N = 1;
// 	// 	} else {
// 	// 		flags.N = 0;
// 	// 	}
// 	// },
// 	'BD': function() { // LDA nnnn, x // Possible playground fall error?
// 		var d = ((rom.file[++register.PC] & 0xff) << 8) | (rom.file[++register.PC] & 0xff);
// 		//console.log(d + register.X);
// 		//clearInterval(dasda);
// 		var addr = Common.Common.swap16(d) + register.X;
// 		createMemoryIfNotExist(addr);
// 		register.A = rom.file[addr]; // rom.file[addr]
// 		console.log('ssssssssss' + register.A);
// 		cpu.setCycle(4); // Add cycle
// 		if(register.A == 0) {
// 			flags.Z = 1;
// 		} else {
// 			flags.Z = 0;
// 		}
// 		if(register.A < 0) {
// 			flags.N = 1;
// 		} else {
// 			flags.N = 0;
// 		}	
// 	},
// 	'C6': function() { // DEC nn
// 		var addr = rom.file[++register.PC];
// 		createMemoryIfNotExist(addr);
// 		memory[addr].value--;
// 		cpu.setCycle(5);
// 		if(memory[addr].value == 0) {
// 			flags.Z = 1;
// 		} else {
// 			flags.Z = 0;
// 		}
// 		if(memory[addr].value < 0) {
// 			flags.N = 1;
// 		} else {
// 			flags.N = 0;
// 		}	
// 	},
// 	'C9': function() {
// 		var comparable = rom.file[++register.PC];
// 		cpu.setCycle(2);
// 		if(register.A - comparable == 0) {
// 			flags.Z = 1;
// 		} else {
// 			flags.Z = 0;
// 		}
// 		if(register.A - comparable < 0) {
// 			flags.N = 1;
// 		} else {
// 			flags.N = 0;
// 		}
// 		if(register.A - comparable >= 0) {
// 			flags.C = 1;
// 		} else {
// 			flags.C = 0;
// 		}
// 	},
// 	'CA': function() { // DEX
// 		register.X--;
// 		cpu.setCycle(2);
// 		if(register.X == 0) {
// 			flags.Z = 1;
// 		} else {
// 			flags.Z = 0;
// 		}
// 		if(register.X < 0) {
// 			flags.N = 1;
// 		} else {
// 			flags.N = 0;
// 		}
// 	},
// 	'D0': function() { // BNE
// 		if(flags.Z == 1) {
// 			register.PC++;
// 			cpu.setCycle(2); // Add cycle
// 		} else {
// 			var num = rom.file[++register.PC];
// 			if (num > 127) {
// 				num = num - 256;
// 			}
// 			cpu.setCycle(3 + (isNextPage(register.PC, register.PC += num)? 1 : 0));
// 		}
// 	},
// 	'D8': function() { // CLD
// 		//flags.D8 = 0;
// 		cpu.setCycle(2);
// 	},
// 	'E6': function() { // INC nn
// 		var addr = rom.file[++register.PC];
// 		createMemoryIfNotExist(addr);
// 		memory[addr].value++;
// 		cpu.setCycle(5);
// 		if(memory[addr].value == 0) {
// 			flags.Z = 1;
// 		} else {
// 			flags.Z = 0;
// 		}
// 		if(memory[addr].value < 0) {
// 			flags.N = 1;
// 		} else {
// 			flags.N = 0;
// 		}	
// 	},
// 	'E8': function() { // INCX
// 		register.X++;
// 		console.log(register.X);
// 		cpu.setCycle(2);
// 		if(register.X == 0) {
// 			flags.Z = 1;
// 		} else {
// 			flags.Z = 0;
// 		}
// 		if(register.X < 0) {
// 			flags.N = 1;
// 		} else {
// 			flags.N = 0;
// 		}
// 	},
// 	'EA': function() { // NOP
// 		cpu.setCycle(2);
// 	},
// 	'F0': function() { // BEQ
// 		if(flags.Z == 0) {
// 			register.PC++;
// 			console.log('jump');
// 			cpu.setCycle(2); // Add cycle
// 		} else {
// 			var num = rom.file[++register.PC];
// 			if (num > 127) {
// 				num = num - 256;
// 			}
// 			console.log('jumped');
// 			cpu.setCycle(3 + (isNextPage(register.PC, register.PC += num)? 1 : 0));
// 		}		
// 	} 
// };